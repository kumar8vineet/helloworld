package com.example.as954.liftprogramming;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import java.util.ArrayList;
import java.util.Collections;


public class LiftPrograming extends AppCompatActivity {


    ArrayList<Integer>upfloor = new ArrayList<Integer>();
    ArrayList<Integer>dnfloor = new ArrayList<Integer>();


    Button start, groundfl, floor10, floor11, floor20, floor21, floor30, floor31, floor40, floor41, floor50, floor51, topfl;


    ImageView gnfl,fl1,fl2,fl3,fl4,fl5,tpfl;

    int i;
    boolean ans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lift_programing);

        final Button start = (Button) findViewById(R.id.start);
        final Button groundfl = (Button) findViewById(R.id.groundfl);
        final Button floor10 = (Button) findViewById(R.id.floor10);
        final Button floor11 = (Button) findViewById(R.id.floor11);
        final Button floor20 = (Button) findViewById(R.id.floor20);
        final Button floor21 = (Button) findViewById(R.id.floor21);
        final Button floor30 = (Button) findViewById(R.id.floor30);
        final Button floor31 = (Button) findViewById(R.id.floor31);
        final Button floor40 = (Button) findViewById(R.id.floor40);
        final Button floor41 = (Button) findViewById(R.id.floor41);
        final Button floor50 = (Button) findViewById(R.id.floor50);
        final Button floor51 = (Button) findViewById(R.id.floor51);
        final Button topfl = (Button) findViewById(R.id.topfl);


        final ImageView gnfl = (ImageView) findViewById(R.id.gnfl);
        final ImageView fl1 = (ImageView) findViewById(R.id.fl1);
        final ImageView fl2 = (ImageView) findViewById(R.id.fl2);
        final ImageView fl3 = (ImageView) findViewById(R.id.fl3);
        final ImageView fl4 = (ImageView) findViewById(R.id.fl4);
        final ImageView fl5 = (ImageView) findViewById(R.id.fl5);
        final ImageView tpfl = (ImageView) findViewById(R.id.tpfl);








        groundfl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gnfl.setImageResource(R.mipmap.uparr);
                dnfloor.add(0);
                groundfl.setEnabled(false);
            }
        });



        floor10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dnfloor.add(1);
                ans = upfloor.contains(1);
                if(ans)
                    fl1.setImageResource(R.mipmap.updn);
                else
                    fl1.setImageResource(R.mipmap.dwnarr);
                floor10.setEnabled(false);
            }
        });



        floor11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upfloor.add(1) ;
                ans = dnfloor.contains(1);
                if(ans)
                    fl1.setImageResource(R.mipmap.updn);
                else
                    fl1.setImageResource(R.mipmap.uparr);
                floor11.setEnabled(false);
            }
        });



        floor20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dnfloor.add(2);
                ans = upfloor.contains(2);
                if(ans)
                    fl2.setImageResource(R.mipmap.updn);
                else
                    fl2.setImageResource(R.mipmap.dwnarr);
                floor20.setEnabled(false);
            }
        });



        floor21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upfloor.add(2);
                ans = dnfloor.contains(2);
                if (ans)
                    fl2.setImageResource(R.mipmap.updn);
                else
                    fl2.setImageResource(R.mipmap.uparr);
                floor21.setEnabled(false);
            }
        });



        floor30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dnfloor.add(3);
                ans = upfloor.contains(3);
                if (ans)
                    fl3.setImageResource(R.mipmap.updn);
                else
                    fl3.setImageResource(R.mipmap.dwnarr);
                floor30.setEnabled(false);
            }
        });



        floor31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upfloor.add(3);
                ans = dnfloor.contains(3);
                if (ans)
                    fl3.setImageResource(R.mipmap.updn);
                else
                    fl3.setImageResource(R.mipmap.uparr);
                floor31.setEnabled(false);
            }
        });



        floor40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dnfloor.add(4);
                ans = upfloor.contains(4);
                if (ans)
                    fl4.setImageResource(R.mipmap.updn);
                else
                    fl4.setImageResource(R.mipmap.dwnarr);
                floor40.setEnabled(false);
            }
        });



        floor41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upfloor.add(4);
                ans = dnfloor.contains(4);
                if (ans)
                    fl4.setImageResource(R.mipmap.updn);
                else
                    fl4.setImageResource(R.mipmap.uparr);
                floor41.setEnabled(false);
            }
        });



        floor50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dnfloor.add(5);
                ans = upfloor.contains(5);
                if (ans)
                    fl5.setImageResource(R.mipmap.updn);
                else
                    fl5.setImageResource(R.mipmap.dwnarr);
                floor50.setEnabled(false);
            }
        });



        floor51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upfloor.add(5);
                ans = dnfloor.contains(5);
                if (ans)
                    fl5.setImageResource(R.mipmap.updn);
                else
                    fl5.setImageResource(R.mipmap.uparr);
                floor51.setEnabled(false);
            }
        });



        topfl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tpfl.setImageResource(R.mipmap.dwnarr);
                upfloor.add(6);
                topfl.setEnabled(false);
            }
        });






        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collections.sort(upfloor);
                Collections.sort(dnfloor) ;

                Collections.reverse(dnfloor);

                upfloor.addAll(dnfloor);

                Integer[] a = upfloor.toArray(new Integer[0]);

                for( i=0; i < a.length ; i++ )
                {
                    switch (a[i]) {
                        case 1:
                            fl1.setImageResource(R.mipmap.flr);
                            break;
                        case 2:
                            fl2.setImageResource(R.mipmap.flr);
                            break;
                        case 3:
                            fl3.setImageResource(R.mipmap.flr);
                            break;
                        case 4:
                            fl4.setImageResource(R.mipmap.flr);
                            break;
                        case 5:
                            fl5.setImageResource(R.mipmap.flr);
                            break;
                        case 6:
                            tpfl.setImageResource(R.mipmap.flr);
                            break;
                        case 0:
                            gnfl.setImageResource(R.mipmap.flr);
                            break;
                    }
                }

                System.out.println("====floorlist" +upfloor.toString());
                Intent intent = new Intent(LiftPrograming.this, OutputActivity.class);
                intent.putExtra("key", upfloor.toString());
                startActivity(intent);

            }
        });


    }


}
