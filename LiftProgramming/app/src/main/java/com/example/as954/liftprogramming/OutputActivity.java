package com.example.as954.liftprogramming;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class OutputActivity extends AppCompatActivity {

    TextView output_msg;
    Button buttonBack, button2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_output);

        TextView output_msg = (TextView)findViewById(R.id.output);
        Button buttonBack = (Button)findViewById(R.id.buttonBack);

        Intent intent = getIntent();

        String output=intent.getStringExtra("key");

        System.out.println("=====floorlist OutPut"+output);

       output_msg.setText(output);

       buttonBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(OutputActivity.this,LiftPrograming.class));

            }
        });

        button2.setOnClickListener((v);
    }
}
